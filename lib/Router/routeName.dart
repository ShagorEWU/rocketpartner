class RouteName {
  static String splashScreen = "/splashScreen";
  static String loginScreen = "/login";
  static String registrationScreen = "/registration";
  static String resetPasswordScreen = "/resetPassword";
  static String homePageScreen = "/bottomNavBarHomePage";
  static String onBoardScreen = "/onBoardScreen";
  static String optScreen = "/otp";
  static String orderScreen = "/orderScreen";
  static String menuScreen = "/menuScreen";
  static String earningScreen = "/earningScreen";
  static String profileScreen = "/profileScreen";
  static String addMenuList = "/addMenuList";
  static String addMenuListAnother = "/addMenuListAnother";
  static String addMenuListImage = "/addMenuListImage";
  static String accountInformation = "/accountInformation";
}
