import 'package:get/get.dart';
import 'package:rocketPartner/Presentation/Earning/earning_screen.dart';
import 'package:rocketPartner/Presentation/Menu/components/add_menu_list.dart';
import 'package:rocketPartner/Presentation/Menu/components/add_menu_list_another.dart';
import 'package:rocketPartner/Presentation/Menu/components/add_menu_list_image.dart';
import 'package:rocketPartner/Presentation/Profile/components/account_information.dart';
import 'package:rocketPartner/Router/routeName.dart';

import '../Common/Widgets/BottomNavBar/bottomNavBar.dart';
import '../Presentation/Login/login_screen.dart';
import '../Presentation/Menu/menu_screen.dart';
import '../Presentation/OTP/otp_screen.dart';
import '../Presentation/OnBoardScreen/on_board_screen.dart';
import '../Presentation/Order/order_screen.dart';
import '../Presentation/Registration/registration_screen.dart';
import '../Presentation/ResetPassword/reset_password_screen.dart';
import '../Presentation/SplashScreen/splash_screen.dart';
import '../Presentation/profile/profile_screen.dart';

class Routes {
  static final route = [
    GetPage(
      name: RouteName.splashScreen,
      page: () => SplashScreen(),
    ),
    GetPage(
      name: RouteName.loginScreen,
      page: () => LoginScreen(),
    ),
    GetPage(
      name: RouteName.registrationScreen,
      page: () => RegistrationScreen(),
    ),
    GetPage(
      name: RouteName.resetPasswordScreen,
      page: () => ResetPasswordScreen(),
    ),
    GetPage(
      name: RouteName.optScreen,
      page: () => OTPScreen(),
    ),
    GetPage(
      name: RouteName.homePageScreen,
      page: () => BottomNavBar(
        activeBar: 0,
      ),
    ),
    GetPage(
      name: RouteName.profileScreen,
      page: () => ProfileScreen(),
    ),
    GetPage(
      name: RouteName.orderScreen,
      page: () => OrderScreen(),
    ),
    GetPage(
      name: RouteName.menuScreen,
      page: () => MenuScreen(),
    ),
    GetPage(
      name: RouteName.onBoardScreen,
      page: () => OnBoardScreen(),
    ),
    GetPage(
      name: RouteName.earningScreen,
      page: () => EarningScreen(),
    ),
    GetPage(
      name: RouteName.addMenuListImage,
      page: () => AddMenuListImage(),
    ),
    GetPage(
      name: RouteName.addMenuListAnother,
      page: () => AddMenuListAnother(),
    ),
    GetPage(
      name: RouteName.addMenuList,
      page: () => AddMenuList(),
    ),
    GetPage(
      name: "/accountInformation",
      page: () => AccountInformation(),
    ),
  ];
}
