import 'package:flutter/material.dart';
import 'package:simple_animations/simple_animations.dart';

enum _animationProperty {opacity, translateX}

class FadeAnimationSlide2Left extends StatelessWidget {
  final double delay;
  final Widget child;
  final double beginPosition;

  const FadeAnimationSlide2Left({Key key, this.delay, this.child, this.beginPosition}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final tween = MultiTween<_animationProperty>()
      ..add(_animationProperty.opacity, Tween(begin: 0.0, end: 1.0))
      ..add(_animationProperty.translateX, Tween(begin: beginPosition != null ? beginPosition : -30.0, end: 0.0));

    return PlayAnimation<MultiTweenValues<_animationProperty>>(
      delay: Duration(milliseconds: (300 * delay).round()),
      duration: tween.duration,
      tween: tween,
      child: child,
      builder: (context, child, value) => Opacity(
        opacity: value.get(_animationProperty.opacity),
        child: Transform.translate(
          offset: Offset(value.get(_animationProperty.translateX), 0),
          child: child,
        ),
      ),
    );
  }
}