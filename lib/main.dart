import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:flutter/foundation.dart' as Foundation;
import 'Common/DependencyInjection/dependencyInjection.dart';
import 'Router/routeName.dart';
import 'Router/routes.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      // statusBarColor is used to set Status bar color in Android devices.
      statusBarColor: Colors.transparent,

      // To make Status bar icons color white in Android devices.
      statusBarIconBrightness: Brightness.dark,

      // statusBarBrightness is used to set Status bar icon color in iOS.
      statusBarBrightness: Brightness.light
      // Here dark means light color Status bar icons.
      ));

  runApp(ScreenUtilInit(
    designSize: Size(360, 690),
    allowFontScaling: false,
    child: GetMaterialApp(
      defaultTransition: Transition.fadeIn,
      initialBinding: DependencyInjection(),
      transitionDuration: Duration(milliseconds: 200),
      enableLog: Foundation.kDebugMode ? true : false,
      initialRoute: RouteName.splashScreen,
      getPages: Routes.route,
      theme: ThemeData(
        fontFamily: 'ubuntu',
      ),
      debugShowCheckedModeBanner: false,
    ),
  ));
}
