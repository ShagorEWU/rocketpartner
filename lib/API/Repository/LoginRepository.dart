import 'package:get/get.dart';
import 'package:rocketPartner/API/Provider/LoginApiProvider.dart';
import 'package:rocketPartner/Model/loginModel/LoginModel.dart';

class LoginRepository {
  LoginApiProvider loginApiProvider = Get.find();

  checkLogin(Map<String, dynamic> map) {
    return loginApiProvider.checkLogin(map);
  }
}
