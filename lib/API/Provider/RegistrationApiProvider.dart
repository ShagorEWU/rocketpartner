import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:rocketPartner/Model/RegistrationModel/RegistrationModel.dart';
import 'package:rocketPartner/Model/loginModel/LoginModel.dart';

import '../ApiConf.dart';

class RegistrationApiProvider {
  Dio dio;
  ApiConf apiConf = new ApiConf();

  RegistrationApiProvider() {
    dio = apiConf.getDio();
  }

  Future checkRegistration(Map<String, dynamic> map) async {
    try {
      Response response =
          await dio.post('post/read.php?action=setNewAdmin', data: map);

      return RegistrationModel.fromJson(response.data);
      //The model class returns data with json.decode().
    } on DioError catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return RegistrationModel.withError(apiConf.handleError(error));
    }
  }
}
