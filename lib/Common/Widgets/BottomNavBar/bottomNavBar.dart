import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
//import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rocketPartner/Presentation/Menu/menu_screen.dart';
import 'package:rocketPartner/Presentation/Order/order_screen.dart';
import 'package:rocketPartner/Presentation/Profile/profile_screen.dart';
import 'package:rocketPartner/Router/routeName.dart';
import 'package:rocketPartner/Router/routes.dart';

BuildContext testContext;

class BottomNavBar extends StatefulWidget {
  const BottomNavBar({
    Key key,
    @required this.activeBar,
  }) : super(key: key);

  final int activeBar;
  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  int _currentIndex;
  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
    _currentIndex == 0
        ? Get.toNamed(RouteName.orderScreen)
        : _currentIndex == 1
            ? Get.toNamed(RouteName.menuScreen)
            : _currentIndex == 2
                ? Get.toNamed(RouteName.earningScreen)
                : Get.toNamed(RouteName.profileScreen);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _currentIndex = widget.activeBar;
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      onTap: onTabTapped,
      currentIndex: _currentIndex,
      selectedItemColor: Colors.green,
      unselectedItemColor: Colors.black,
      items: [
        BottomNavigationBarItem(
            icon: Icon(FlutterIcons.first_order_faw), label: "Order"),
        BottomNavigationBarItem(
            icon: Icon(FlutterIcons.ios_menu_ion), label: "Menu"),
        BottomNavigationBarItem(
            icon: Icon(FlutterIcons.dollar_faw), label: "Earning"),
        BottomNavigationBarItem(icon: Icon(Icons.people), label: "Profile"),
      ],
    );
  }
}
