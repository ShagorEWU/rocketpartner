import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:rocketPartner/API/Provider/LoginApiProvider.dart';
import 'package:rocketPartner/API/Provider/RegistrationApiProvider.dart';
import 'package:rocketPartner/API/Repository/LoginRepository.dart';
import 'package:rocketPartner/API/Repository/RegistrationRepository.dart';

import 'package:rocketPartner/Common/Methods/CommonMethod.dart';
import 'package:rocketPartner/Common/Methods/Notifications.dart';
import 'package:rocketPartner/Common/Storage/storagePref.dart';
import 'package:rocketPartner/Router/routes.dart';

class DependencyInjection implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<Notifications>(() => Notifications(), fenix: true);
    Get.lazyPut<CommonMethod>(() => CommonMethod(), fenix: true);
    Get.lazyPut<LoginRepository>(() => LoginRepository(), fenix: true);
    Get.lazyPut<LoginApiProvider>(() => LoginApiProvider(), fenix: true);
    Get.lazyPut<RegistrationRepository>(() => RegistrationRepository(),
        fenix: true);
    Get.lazyPut<RegistrationApiProvider>(() => RegistrationApiProvider(),
        fenix: true);
    Get.lazyPut<GetStorage>(() => GetStorage(), fenix: true);
    Get.lazyPut<StoragePref>(() => StoragePref(), fenix: true);
    Get.lazyPut<Routes>(() => Routes(), fenix: true);
  }
}
