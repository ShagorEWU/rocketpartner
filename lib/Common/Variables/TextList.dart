import 'package:flutter/material.dart';

// ignore: camel_case_types
class CustomText {
  static Text textRemember = Text("Remember Me  ",
      style: TextStyle(
        color: Colors.black,
        fontSize: 12.0,
      ));

  static Text textRegistration = Text("REGISTRATION",
      style: TextStyle(
        color: Colors.white,
        fontSize: 14.0,
      ));

  static Text textRegistrationn = Text("Registration",
      style: TextStyle(
        color: Colors.green,
        fontSize: 14.0,
      ));
  static Text textLogo = Text("LOGO",
      style: TextStyle(
          fontSize: 15.0, letterSpacing: .6, fontWeight: FontWeight.bold));
  static const Text textSignIn = Text("LOGIN",
      style: TextStyle(color: Colors.white, fontSize: 18, letterSpacing: 1.0));
  static const Text textContinue = Text("CONTINUE",
      style: TextStyle(color: Colors.white, fontSize: 18, letterSpacing: 1.0));
  static const Text textGoogleSign = Text("Login With Google",
      style: TextStyle(
        color: Color(0xFF188A2B),
      ));
  static const Text newUser = Text(
    "New User? ",
    style: TextStyle(),
  );
  static const Text textSocialLogin = Text("Social Login",
      style: TextStyle(
        fontSize: 16.0,
      ));

  static Text textPhoneNumber = Text("PhoneNumber",
      style: TextStyle(fontFamily: "Poppins-Medium", fontSize: 13.0));
  static Text textLogin = Text("Username",
      style: TextStyle(fontFamily: "Poppins-Medium", fontSize: 13.0));
  static Text textEmail = Text("email",
      style: TextStyle(fontFamily: "Poppins-Medium", fontSize: 13.0));
  static Text textPassword = Text("PassWord",
      style: TextStyle(fontFamily: "Poppins-Medium", fontSize: 13.0));
  static Text textNewPassword = Text("New PassWord",
      style: TextStyle(fontFamily: "Poppins-Medium", fontSize: 13.0));
  static Text textConfirmPassword = Text("ConfirmPassWord",
      style: TextStyle(fontFamily: "Poppins-Medium", fontSize: 13.0));
  static Text textForgetPassword = Text(
    "Forgot Password?",
    style: TextStyle(color: Colors.green, fontSize: 14.0),
  );
  static Text textLoginFromRegistrationPage = Text(
    "Login",
    style: TextStyle(color: Colors.green, fontSize: 14.0),
  );
  static Text textTermsCondition = Text(
    "Accept Terms & Conditions",
    style: TextStyle(color: Colors.black, fontSize: 14.0),
  );
}
