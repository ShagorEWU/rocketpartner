import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

import 'HexColor.dart';
import 'HexColor.dart';

class Notifications {
  Widget spinKitLoader() {
    return SpinKitDoubleBounce(
      color: HexColor("#2980B9"),
      size: 40.0,
    );
  }

  // ignore: non_constant_identifier_names
  void Loader({
    String msg,
  }) {
    Get.dialog(
      Material(
        type: MaterialType.transparency,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SpinKitCircle(
                color: HexColor("#D3D3D3"),
                size: 50.0,
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                msg,
                style: TextStyle(
                  fontSize: 14,
                  color: HexColor("#AEE110"),
                ),
              ),
            ],
          ),
        ),
      ),
      barrierDismissible: false,
    );
  }

  void dismissLoader() {
    Get.back();
  }

  void snackBarDialog(
      {String message,
      IconData icon,
      String title,
      String textColor,
      String iconColor,
      String bgColor,
      SnackPosition position}) {
    Get.snackbar(
      title != null ? title : null, // Title
      message != null ? message : "Message", // Message
      colorText: textColor != null ? HexColor(textColor) : HexColor("#FFFFFF"),
      icon: Icon(
        icon,
        size: 30.0,
        color: iconColor != null ? HexColor(iconColor) : HexColor("#FFFFFF"),
      ),
      backgroundColor:
          bgColor != null ? HexColor(bgColor) : HexColor("#b00000"),
      shouldIconPulse: true,
      barBlur: 0,
      isDismissible: true,
      duration: Duration(seconds: 3),
      snackPosition: position == null
          ? position = SnackPosition.BOTTOM
          : position = SnackPosition.TOP,
      borderRadius: 0,
      margin: EdgeInsets.all(0),
      padding: EdgeInsets.only(left: 30, right: 15, top: 15, bottom: 15),
      snackStyle: SnackStyle.GROUNDED,
      forwardAnimationCurve: Curves.fastOutSlowIn,
      reverseAnimationCurve: Curves.fastOutSlowIn,
    );
  }

  void snackBarDialogProgress(
      {String message,
      String title,
      String textColor,
      String iconColor,
      String bgColor,
      SnackPosition position,
      String progressColor,
      String animationColor}) {
    Get.snackbar(
      title != null ? title : null, // Title
      message != null ? message : "Message", // Message
      colorText: textColor != null ? HexColor(textColor) : HexColor("#FFFFFF"),
      icon: SpinKitDoubleBounce(
        size: 30.0,
        color: iconColor != null ? HexColor(iconColor) : HexColor("#E44F05"),
      ),
      backgroundColor:
          bgColor != null ? HexColor(bgColor) : HexColor("#232323"),
      shouldIconPulse: false,
      barBlur: 0,
      isDismissible: true,
      snackPosition: position == null
          ? position = SnackPosition.BOTTOM
          : position = SnackPosition.TOP,
      borderRadius: 0,
      margin: EdgeInsets.all(0),
      padding: EdgeInsets.all(20),
      snackStyle: SnackStyle.GROUNDED,
      forwardAnimationCurve: Curves.fastOutSlowIn,
      reverseAnimationCurve: Curves.fastOutSlowIn,
      showProgressIndicator: true,
      progressIndicatorBackgroundColor:
          progressColor != null ? HexColor(progressColor) : HexColor("#2C3E50"),
      progressIndicatorValueColor: new AlwaysStoppedAnimation<Color>(
          animationColor != null
              ? HexColor(animationColor)
              : HexColor("#D35400")),
    );
  }
}
