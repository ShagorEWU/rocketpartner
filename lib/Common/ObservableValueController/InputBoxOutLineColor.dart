import 'package:get/get.dart';
import 'package:rocketPartner/Common/Methods/HexColor.dart';

class InputBoxOutLineColorController extends GetxController {
  final color = HexColor("#249B96").obs;
}
