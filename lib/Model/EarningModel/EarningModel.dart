const EARNING_DATA = [
  {
    "date": "19/01/2021",
    "day": "Tuesday",
    "totalEarnig": "1080",
    "totalOrder": "8"
  },
  {
    "date": "18/01/2021",
    "day": "Monday",
    "totalEarnig": "2054",
    "totalOrder": "10"
  },
  {
    "date": "17/01/2021",
    "day": "Sunday",
    "totalEarnig": "4789",
    "totalOrder": "25"
  },
  {
    "date": "16/01/2021",
    "day": "Saturday",
    "totalEarnig": "555",
    "totalOrder": "5"
  },
  {
    "date": "15/01/2021",
    "day": "Friday",
    "totalEarnig": "4587",
    "totalOrder": "10"
  },
  {
    "date": "14/01/2021",
    "day": "Thursday",
    "totalEarnig": "4782",
    "totalOrder": "15"
  },
  {
    "date": "13/01/2021",
    "day": "Wednesday",
    "totalEarnig": "4545",
    "totalOrder": "14"
  },
];
