// To parse this JSON data, do
//
//     final registrationModel = registrationModelFromJson(jsonString);

import 'dart:convert';

RegistrationModel registrationModelFromJson(String str) =>
    RegistrationModel.fromJson(json.decode(str));

String registrationModelToJson(RegistrationModel data) =>
    json.encode(data.toJson());

class RegistrationModel {
  RegistrationModel({
    this.message,
    this.errorCode,
  });

  String message;
  String errorCode;

  factory RegistrationModel.fromJson(Map<String, dynamic> json) =>
      RegistrationModel(
        message: json["message"],
        errorCode: json["errorCode"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "errorCode": errorCode,
      };

  // ignore: missing_return
  static Future withError(String handleError) {}
}
