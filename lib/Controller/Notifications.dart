import 'package:get/get.dart';

class Notifications {
  final status = true.obs;
}

class NotificationsController extends GetxController {
  NotificationsController();

  final notifications = Notifications().obs;
}
