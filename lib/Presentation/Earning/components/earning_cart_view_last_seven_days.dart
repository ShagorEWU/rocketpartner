import 'package:flutter/material.dart';
import 'package:rocketPartner/Common/Methods/HexColor.dart';

class EarningCardViewLastSevenDays extends StatefulWidget {
  EarningCardViewLastSevenDays({
    Key key,
    @required this.date,
    @required this.day,
    @required this.totalEarning,
    @required this.totalOrder,
  }) : super(key: key);
  final date;
  final day;
  final totalEarning;
  final totalOrder;
  @override
  _EarningCardViewLastSevenDaysState createState() =>
      _EarningCardViewLastSevenDaysState();
}

class _EarningCardViewLastSevenDaysState
    extends State<EarningCardViewLastSevenDays> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.white70,
      ),
      child: Card(
        elevation: 1,
        child: Column(
          children: [
            Card(
              elevation: 0,
              margin: const EdgeInsets.fromLTRB(15, 8, 15, 8),
              child: Center(
                  child: Center(
                      child: Text(
                widget.date,
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              ))),
            ),
            Card(
              elevation: 0,
              margin: const EdgeInsets.fromLTRB(15, 8, 15, 8),
              child: Center(
                  child: Text(
                widget.day,
                style: TextStyle(fontSize: 30, color: Colors.green[900]),
                textAlign: TextAlign.center,
              )),
            ),
            Card(
              elevation: 0,
              margin: const EdgeInsets.fromLTRB(15, 8, 15, 8),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      "Total Earning",
                      style: TextStyle(color: Colors.green),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Expanded(
                    child: Text(
                      widget.totalEarning,
                      style: TextStyle(color: Colors.black),
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              ),
            ),
            Card(
              elevation: 0.5,
              margin: const EdgeInsets.fromLTRB(15, 8, 15, 8),
              child: Row(
                children: [
                  Expanded(
                      child: Text(
                    "Total Order",
                    style: TextStyle(color: Colors.green),
                    textAlign: TextAlign.center,
                  )),
                  Expanded(
                    child: Text(
                      widget.totalOrder,
                      style: TextStyle(color: Colors.black),
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
