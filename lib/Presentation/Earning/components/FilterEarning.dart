import 'package:flutter/material.dart';

class FilterEarning extends StatefulWidget {
  FilterEarning(
      {Key key,
      this.index,
      this.totalEarning,
      this.receivedearning,
      this.pendingearning})
      : super(key: key);
  final index;
  final totalEarning;
  final receivedearning;
  final pendingearning;
  @override
  _FilterEarningState createState() => _FilterEarningState();
}

class _FilterEarningState extends State<FilterEarning> {
  String totalearning, receivedearning, pendingearning;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.fromLTRB(10, 0, 0, 10),
        height: MediaQuery.of(context).size.height - 250,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          //color: Colors.white70,
        ),
        child: Column(children: [
          Card(
            clipBehavior: Clip.antiAlias,
            shadowColor: Colors.grey[350],
            elevation: 2,
            child: Container(
              color: Colors.white,
              height: ((MediaQuery.of(context).size.height - 250.0) / 2.0) - 70,
              width: MediaQuery.of(context).size.width,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Total Earning",
                      style: TextStyle(fontSize: 30, color: Colors.green),
                    ),
                    Text(
                      widget.totalEarning,
                      style: TextStyle(fontSize: 25),
                      textAlign: TextAlign.center,
                    ),
                  ]),
            ),
          ),
          Padding(padding: const EdgeInsets.only(top: 05)),
          Row(children: [
            Expanded(
                child: Column(children: [
              Card(
                clipBehavior: Clip.antiAlias,
                shadowColor: Colors.grey[350],
                elevation: 2,
                child: Container(
                    color: Colors.white,

                    //alignment: Alignment.center,
                    height:
                        ((MediaQuery.of(context).size.height - 250.0) / 2.0) -
                            70,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Received",
                          style: TextStyle(fontSize: 30, color: Colors.green),
                        ),
                        Center(
                            child: Text(
                          widget.receivedearning,
                          style: TextStyle(fontSize: 25),
                        ))
                      ],
                    )),
              ),
            ])),
            Expanded(
              child: Card(
                clipBehavior: Clip.antiAlias,
                shadowColor: Colors.grey[350],
                elevation: 2,
                child: Container(
                  color: Colors.white,
                  height:
                      ((MediaQuery.of(context).size.height - 250.0) / 2.0) - 70,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Pending",
                        style: TextStyle(fontSize: 30, color: Colors.green),
                      ),
                      Text(
                        widget.pendingearning,
                        style: TextStyle(fontSize: 25),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ])
        ]));
  }
}
