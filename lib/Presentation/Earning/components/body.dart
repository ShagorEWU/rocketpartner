import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rocketPartner/Common/Methods/HexColor.dart';

import 'package:rocketPartner/Model/EarningModel/EarningModel.dart';
import '../../order/components/all_order_cards.dart';
import 'FilterEarning.dart';
import 'earning_cart_view_last_seven_days.dart';

/// ignore: camel_case_types
class Body extends StatefulWidget {
  const Body({Key key}) : super(key: key);
  @override
  _BodyState createState() => _BodyState();
}

// ignore: camel_case_types
class _BodyState extends State<Body> with SingleTickerProviderStateMixin {
  int indext;
  @override
  void initState() {
    indext = 0;
    // TODO: implement initState
    super.initState();

    // Create TabController for getting the index of current tab
  }

  @override
  Widget build(BuildContext context) {
    List<dynamic> earningData = EARNING_DATA;
    return Scaffold(
      body: SafeArea(
        child: Container(
          //alignment: Alignment.bottomCenter,
          padding: const EdgeInsets.only(top: 20),
          child: SingleChildScrollView(
            // controller: controller,
            child: Column(children: [
              Container(
                alignment: Alignment.topRight,
                child: TextButton(
                    child: Text(
                      "Filter",
                      style: TextStyle(color: Colors.green, fontSize: 20),
                    ),
                    onPressed: () {
                      Get.bottomSheet(Container(
                        child: SizedBox(
                          height: 300,
                          child: Container(
                            color: HexColor("#232323"),
                            child: Column(children: [
                              RaisedButton(
                                color: Colors.green,
                                child: Text(
                                  "Daily",
                                  style: TextStyle(fontSize: 20),
                                ),
                                onPressed: () {
                                  setState(() {
                                    indext = 0;
                                  });
                                  Get.back();
                                },
                              ),
                              RaisedButton(
                                color: Colors.green,
                                child: Text(
                                  "Weakly",
                                  style: TextStyle(fontSize: 20),
                                ),
                                onPressed: () {
                                  setState(() {
                                    indext = 1;
                                  });
                                  Get.back();
                                },
                              ),
                              RaisedButton(
                                color: Colors.green,
                                child: Text(
                                  "Monthly",
                                  style: TextStyle(fontSize: 20),
                                ),
                                onPressed: () {
                                  setState(() {
                                    indext = 2;
                                  });
                                  Get.back();
                                },
                              ),
                              RaisedButton(
                                color: Colors.green,
                                child: Text(
                                  "Cancel",
                                  style: TextStyle(fontSize: 20),
                                ),
                                onPressed: () {
                                  Get.back();
                                },
                              ),
                            ] //     actions: [
                                ),
                          ),
                        ),
                      ));
                    }),
              ),
              SizedBox(
                // Horizontal ListView
                height: 200,
                child: ListView.builder(
                  itemCount: 7,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: EarningCardViewLastSevenDays(
                          date: earningData[index]["date"],
                          day: earningData[index]["day"],
                          totalEarning: index == 0
                              ? "458"
                              : index == 1
                                  ? "4587"
                                  : index == 2
                                      ? "4455"
                                      : index == 3
                                          ? "5554"
                                          : "5487",
                          totalOrder: index == 0
                              ? "100"
                              : index == 1
                                  ? "120"
                                  : index == 2
                                      ? "50"
                                      : index == 3
                                          ? "45"
                                          : "58",
                        ));
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
              ),
              indext == 0
                  ? FilterEarning(
                      index: 0,
                      totalEarning: "12000",
                      receivedearning: "9000",
                      pendingearning: "3000",
                    )
                  : indext == 1
                      ? FilterEarning(
                          index: 1,
                          totalEarning: "10000",
                          receivedearning: "9000",
                          pendingearning: "1000",
                        )
                      : FilterEarning(
                          index: 2,
                          totalEarning: "15000",
                          receivedearning: "10000",
                          pendingearning: "5000",
                        ),
            ]),
          ),
        ),
      ),
    );
  }
}
