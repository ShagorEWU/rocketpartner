import 'package:flutter/material.dart';
import 'package:rocketPartner/Common/Widgets/BottomNavBar/bottomNavBar.dart';

import 'components/body.dart';

// ignore: camel_case_types
class EarningScreen extends StatefulWidget {
  final BuildContext menuScreenContext;
  final Function onScreenHideButtonPressed;
  final bool hideStatus;
  const EarningScreen(
      {Key key,
      this.menuScreenContext,
      this.onScreenHideButtonPressed,
      this.hideStatus = false})
      : super(key: key);

  @override
  _EarningScreenState createState() => _EarningScreenState();
}

// ignore: camel_case_types
class _EarningScreenState extends State<EarningScreen> {
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(),
      bottomNavigationBar: BottomNavBar(
        activeBar: 2,
      ),
    );
  }
}
