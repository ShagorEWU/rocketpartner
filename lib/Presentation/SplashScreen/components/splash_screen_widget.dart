import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter_spinkit/flutter_spinkit.dart';

// ignore: camel_case_types
class splash_screen_widget extends StatefulWidget {
  @override
  _splash_screen_widgetState createState() => _splash_screen_widgetState();
}

// ignore: camel_case_types
class _splash_screen_widgetState extends State<splash_screen_widget> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 4), () {
      Navigator.pushReplacementNamed(context, "/bottomNavBarHomePage",
          arguments: {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
          // ignore: missing_return
          onWillPop: () {},
          child: Center(
            child: Container(
              color: Colors.black45.withOpacity(0.90),
              child: SimpleDialog(
                elevation: 0.0,
                backgroundColor: Colors.transparent,
                children: <Widget>[
                  Center(
                      child: Container(
                          child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 00.0),
                        child: Text(
                          "Rocket Partner",
                          style: TextStyle(
                              color: Colors.green,
                              fontFamily: "Poppins-Bold",
                              fontSize: 30,
                              letterSpacing: 1.0),
                        ),
                      ),
                      SpinKitDoubleBounce(
                        color: Colors.orange[900],
                        size: 50.0,
                      ),
                      SizedBox(height: 20),
                      Text(
                        "Loading",
                        style: TextStyle(color: Colors.blue[300]),
                      )
                    ],
                  ))),
                ],
              ),
            ),
          )),
    );
  }
}
