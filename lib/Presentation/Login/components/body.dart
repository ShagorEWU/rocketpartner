import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rocketPartner/Common/Variables/TextList.dart';
import 'package:rocketPartner/Router/routeName.dart';

import 'package:rocketPartner/Router/routes.dart';

import 'login_form_card.dart';
//import 'package:flutter_screenutil/flutter_screenutil.dart';

// ignore: camel_case_types
class body extends StatefulWidget {
  @override
  _bodyState createState() => new _bodyState();
}

// ignore: camel_case_types
class _bodyState extends State<body> {
  bool _isSelected = false;
  TextEditingController userName = new TextEditingController();
  TextEditingController password = new TextEditingController();
  void _radio() {
    setState(() {
      _isSelected = !_isSelected;
    });
  }

  @override
  Widget build(BuildContext context) {
    // ScreenUtil.init(context);
    // ScreenUtil.init(context,
    // designSize: Size(750, 1334), allowFontScaling: true);
    return new Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: true,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Expanded(
                child: Container(),
              ),
            ],
          ),
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: 28.0, right: 28.0, top: 60.0),
              child: Column(
                children: <Widget>[
                  Text(userName.text.toString()),
                  // Row(
                  //   children: <Widget>[
                  //     Container(
                  //       width: ScreenUtil().setWidth(110),
                  //       height: ScreenUtil().setHeight(110),
                  //     ),
                  //     //CustomText.textLogo,
                  //   ],
                  // ),
                  SizedBox(
                    height: 180,
                    child: FormCard(
                      UserName: userName,
                      Password: password,
                    ),
                  ),
                  //Login Form Card
                  // SizedBox(height: ScreenUtil().setHeight(40)),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          SizedBox(
                            width: 8.0,
                          ),
                          InkWell(
                            onTap: () {
                              Get.toNamed(RouteName.registrationScreen);
                            },
                            child: Center(
                              child: CustomText.textRegistrationn,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20, // specific value
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              side: BorderSide(color: Colors.green)),
                          onPressed: () {
                            Get.toNamed(RouteName.optScreen);
                          },
                          color: Colors.green,
                          textColor: Colors.white,
                          child:
                              Text("Login", style: TextStyle(fontSize: 14.0)),
                        ),
                      ),
                    ],
                  ),
                  // SizedBox(
                  //   height: ScreenUtil().setHeight(40),
                  // ),

                  // SizedBox(
                  //   height: ScreenUtil().setHeight(30),
                  // ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CustomText.newUser,
                      InkWell(
                        onTap: () {},
                        child: CustomText.textGoogleSign,
                      )
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
