import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:rocketPartner/API/Repository/LoginRepository.dart';
import 'package:rocketPartner/Common/Methods/HexColor.dart';
import 'package:rocketPartner/Common/Methods/Notifications.dart';
import 'package:rocketPartner/Common/Storage/storagePref.dart';

import 'package:rocketPartner/Model/loginModel/LoginModel.dart';
import 'package:rocketPartner/Router/routeName.dart';
import 'package:rocketPartner/Router/routes.dart';

class LoginBloc extends GetxController {
  //final c = Get.put(UserController());
  LoginRepository repository = Get.find();
  StoragePref storagePref = Get.find();

  Notifications notifications = Get.find();
  //bool isAuthinticated;
  @override
  void onInit() {
    super.onInit();
    //isAuthinticated = false;
  }

  void submitLoginData(Map<String, dynamic> map) async {
    notifications.Loader(msg: "Signing IN");

    LoginModel response = await repository.checkLogin(map);

    if (response.errorCode == "0") {
      storagePref.storageWrite("email", response.data[0].email);
      storagePref.storageWrite("phoneNumber", response.data[0].phoneNumber);
      storagePref.storageWrite("userName", response.data[0].adminName);

      notifications.dismissLoader();
      Get.toNamed(RouteName.optScreen);
    } else {
      notifications.dismissLoader();

      notifications.snackBarDialog(
          message: "Invalid Input",
          icon: FlutterIcons.warning_faw,
          position: SnackPosition.TOP);
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
