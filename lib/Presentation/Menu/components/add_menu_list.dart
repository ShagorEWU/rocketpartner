import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:get/get.dart';
import 'package:rocketPartner/Common/Widgets/Button/radioButton.dart';
import 'package:rocketPartner/Presentation/Menu/components/meal_type_card.dart';
import 'package:rocketPartner/Presentation/Menu/components/time_period_card.dart';
import 'package:rocketPartner/Router/routeName.dart';

import 'add_menu_list_another.dart';

class AddMenuList extends StatefulWidget {
  AddMenuList({Key key}) : super(key: key);

  @override
  _AddMenuListState createState() => _AddMenuListState();
}

class _AddMenuListState extends State<AddMenuList> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Get.back();
            }),
      ),
      body: SingleChildScrollView(
          // controller: controller,

          child: Container(
        padding: const EdgeInsets.all(20),
        child: Column(children: [
          TimePeriodCard(),
          MealTypeCard(),
        ]),
      )),
      bottomNavigationBar: SizedBox(
        height: ScreenUtil().setHeight(50), // specific value
        child: RaisedButton(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0)),
              side: BorderSide(color: Colors.green)),
          onPressed: () {
            Get.toNamed(RouteName.addMenuListAnother);
            //print(_loginBloc.emailTextController.text);
          },
          color: Colors.green,
          textColor: Colors.white,
          child: Text('Continue'),

          //

          //
        ),
      ),
    );
  }
}
