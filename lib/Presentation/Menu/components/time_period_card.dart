import 'package:flutter/material.dart';

class TimePeriodCard extends StatefulWidget {
  TimePeriodCard({Key key}) : super(key: key);

  @override
  _TimePeriodCardState createState() => _TimePeriodCardState();
}

class _TimePeriodCardState extends State<TimePeriodCard> {
  int _groupValue = -1;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(25)),
      child: Card(
          clipBehavior: Clip.antiAlias,
          shadowColor: Colors.grey[350],
          elevation: 10,
          child: Column(
            children: [
              Text(
                "Time Period",
                style: TextStyle(fontSize: 30),
              ),
              Column(
                children: [
                  Row(
                    children: [
                      new Radio(
                        value: 0,
                        groupValue: _groupValue,
                        onChanged: (newValue) =>
                            setState(() => _groupValue = newValue),
                      ),
                      new Text(
                        'Break Fast',
                        style:
                            new TextStyle(fontSize: 12.0, color: Colors.black),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      new Radio(
                        value: 1,
                        groupValue: _groupValue,
                        onChanged: (newValue) =>
                            setState(() => _groupValue = newValue),
                      ),
                      new Text(
                        'Launch',
                        style:
                            new TextStyle(fontSize: 12.0, color: Colors.black),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      new Radio(
                        value: 2,
                        groupValue: _groupValue,
                        onChanged: (newValue) =>
                            setState(() => _groupValue = newValue),
                      ),
                      new Text(
                        'Dinner',
                        style:
                            new TextStyle(fontSize: 12.0, color: Colors.black),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          )),
    );
  }
}
