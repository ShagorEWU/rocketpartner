import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rocketPartner/Presentation/Order/components/all_order_body.dart';

class ShowBreakFastLaunchOrDinnerFullMunuList extends StatelessWidget {
  ShowBreakFastLaunchOrDinnerFullMunuList({Key key, @required this.txt})
      : super(key: key);
  final txt;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Get.back();
              }),
          title: Text(txt),
        ),
        body: txt == "Break Fast"
            ? SingleChildScrollView(child: AllOrderBody())
            : txt == "Launch"
                ? SingleChildScrollView(child: AllOrderBody())
                : SingleChildScrollView(child: AllOrderBody()));
  }
}
