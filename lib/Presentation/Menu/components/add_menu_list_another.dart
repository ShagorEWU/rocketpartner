import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:get/route_manager.dart';
import 'package:rocketPartner/Presentation/Registration/components/registration_form_card.dart';
import 'package:rocketPartner/Router/routeName.dart';

import 'add_menu_another_list_card.dart';
import 'add_menu_list_image.dart';

class AddMenuListAnother extends StatefulWidget {
  AddMenuListAnother({Key key}) : super(key: key);

  @override
  _AddMenuListAnotherState createState() => _AddMenuListAnotherState();
}

class _AddMenuListAnotherState extends State<AddMenuListAnother> {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(360, 690),
      allowFontScaling: false,
      child: new Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomPadding: true,
        body: SafeArea(
          left: false,
          top: true,
          bottom: false,
          right: false,
          child: NestedScrollView(
            //controller: _scrollViewController,
            headerSliverBuilder: (BuildContext context, bool boxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  leading: IconButton(
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {
                        Get.back();
                      }),
                  expandedHeight: 100.0,
                  floating: true,
                  pinned: true,
                  elevation: 1,
                  backgroundColor: Colors.white,
                  iconTheme: IconThemeData(
                    color: Colors.black45, //change your color here
                  ),
                  //flexibleSpace: MyAppSpace()
                )
              ];
            },
            body: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.only(left: 40.0, right: 40.0, top: 0.0),
                child: Column(
                  children: <Widget>[

                   
                  ],
                ),
              ),
            ),
          ),
        ),
        bottomNavigationBar: SizedBox(
          height: ScreenUtil().setHeight(50), // specific value
          child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0)),
                side: BorderSide(color: Colors.green)),
            onPressed: () {
              //print(_loginBloc.emailTextController.text);

              Get.toNamed(RouteName.addMenuListImage);
            },
            color: Colors.green,
            textColor: Colors.white,
            child: Text('Registration'),

            //

            //
          ),
        ),
      ),
    );return new Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: true,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: 28.0, right: 28.0, top: 60.0),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 00.0),
                    child: Image.asset("asset/login.jpg"),
                  ),

                  AddMenuAnotherListCard(), //Reset Form Card
                ],
              ),
            ),
          )
        ],
      ),
      bottomNavigationBar: SizedBox(
        height: ScreenUtil().setHeight(50), // specific value
        child: RaisedButton(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0)),
              side: BorderSide(color: Colors.green)),
          onPressed: () {
            //print(_loginBloc.emailTextController.text);

            Get.toNamed(RouteName.menuScreen);
          },
          color: Colors.green,
          textColor: Colors.white,
          child: Text('Submit'),

          //

          //
        ),
      ),
    );
  }
}
