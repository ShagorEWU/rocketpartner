import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rocketPartner/Model/predefinedFoods.dart';
import 'package:rocketPartner/Presentation/Order/components/all_order_body.dart';

import 'Show_breakFast_Laounch_Or_Dinner_Full_Menu_list.dart';

class ShowMenuList extends StatefulWidget {
  @override
  _ShowMenuListState createState() => _ShowMenuListState();
}

class _ShowMenuListState extends State<ShowMenuList> {
  List<dynamic> responseList = FOOD_DATA;
  @override
  Widget build(BuildContext context) {
    return Container(
        // height: 370,

        // margin: EdgeInsets.symmetric(
        //   horizontal: 16,
        //   vertical: 8,
        // ),
        child: SingleChildScrollView(
      //controller: controller,
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Text(
                    "Break Fast",
                    style: TextStyle(fontSize: 20, color: Colors.green),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.centerRight,
                  padding: const EdgeInsets.only(left: 20.0),
                  child: TextButton(
                    child: Text("See All ( ${responseList.length} )"),
                    onPressed: () {
                      Get.to(ShowBreakFastLaunchOrDinnerFullMunuList(
                          txt: "Break Fast"));
                    },
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            // Horizontal ListView
            height: 150,
            child: ListView.builder(
              itemCount: responseList.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                        height: 140,
                        width: 220,
                        child: Container(
                          //color: Colors.lightGreen,
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 10.0),
                          decoration: BoxDecoration(
                            color: Colors.lightGreen,
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    responseList[index]["name"],
                                    style: const TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    responseList[index]["brand"],
                                    style: const TextStyle(
                                        fontSize: 10, color: Colors.grey),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "\$ ${responseList[index]["price"]}",
                                    style: const TextStyle(
                                        fontSize: 20,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                              Image.asset(
                                "asset/${responseList[index]["image"]}",
                                height: 100,
                              )
                            ],
                          ),
                        )),
                  ),
                );
              },
            ),
          ),
          Padding(padding: const EdgeInsets.only(bottom: 18.0)),
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Text(
                    "Launch",
                    style: TextStyle(fontSize: 20, color: Colors.green),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.centerRight,
                  padding: const EdgeInsets.only(left: 20.0),
                  child: TextButton(
                    child: Text("See All ( ${responseList.length} )"),
                    onPressed: () {
                      Get.to(ShowBreakFastLaunchOrDinnerFullMunuList(
                          txt: "Launch"));
                    },
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            // Horizontal ListView
            height: 150,
            child: ListView.builder(
              itemCount: responseList.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                        height: 140,
                        width: 220,
                        child: Container(
                          //color: Colors.lightGreen,
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 10.0),
                          decoration: BoxDecoration(
                            color: Colors.lightGreen,
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    responseList[index]["name"],
                                    style: const TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    responseList[index]["brand"],
                                    style: const TextStyle(
                                        fontSize: 10, color: Colors.grey),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "\$ ${responseList[index]["price"]}",
                                    style: const TextStyle(
                                        fontSize: 20,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                              Image.asset(
                                "asset/${responseList[index]["image"]}",
                                height: 100,
                              )
                            ],
                          ),
                        )),
                  ),
                );
              },
            ),
          ),
          Padding(padding: const EdgeInsets.only(bottom: 18.0)),
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Text(
                    "Dinner",
                    style: TextStyle(fontSize: 20, color: Colors.green),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.centerRight,
                  padding: const EdgeInsets.only(left: 20.0),
                  child: TextButton(
                    child: Text("See All ( ${responseList.length} )"),
                    onPressed: () {
                      Get.to(ShowBreakFastLaunchOrDinnerFullMunuList(
                          txt: "Dinner"));
                    },
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            // Horizontal ListView
            height: 150,
            child: ListView.builder(
              itemCount: responseList.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                        height: 140,
                        width: 220,
                        child: Container(
                          //color: Colors.lightGreen,
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 10.0),
                          decoration: BoxDecoration(
                            color: Colors.lightGreen,
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    responseList[index]["name"],
                                    style: const TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    responseList[index]["brand"],
                                    style: const TextStyle(
                                        fontSize: 10, color: Colors.grey),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "\$ ${responseList[index]["price"]}",
                                    style: const TextStyle(
                                        fontSize: 20,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                              Image.asset(
                                "asset/${responseList[index]["image"]}",
                                height: 100,
                              )
                            ],
                          ),
                        )),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    ));
  }
}
