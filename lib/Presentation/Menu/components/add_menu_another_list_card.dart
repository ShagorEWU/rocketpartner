import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';

class AddMenuAnotherListCard extends StatefulWidget {
  AddMenuAnotherListCard({Key key}) : super(key: key);

  @override
  _AddMenuAnotherListCardState createState() => _AddMenuAnotherListCardState();
}

class _AddMenuAnotherListCardState extends State<AddMenuAnotherListCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8.0),
          boxShadow: [
            BoxShadow(
                color: Colors.white,
                offset: Offset(0.0, 15.0),
                blurRadius: 15.0),
            BoxShadow(
                color: Colors.white,
                offset: Offset(0.0, -10.0),
                blurRadius: 10.0),
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(children: [
            Expanded(
                child: SizedBox(
              height: 55,
              width: MediaQuery.of(context).size.width - 56.0,
              child: TextFormField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  hintText: "Menu Name",
                  border: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(15.0),
                    borderSide: new BorderSide(),
                  ),
                ),
              ),
            ))
          ]),
          Padding(padding: const EdgeInsets.all(20)),
          Row(children: [
            Expanded(
                child: SizedBox(
              height: 55,
              width: (MediaQuery.of(context).size.width - 56.0 - 10.0) / 2.0,
              child: TextFormField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  hintText: "Price",
                  border: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(15.0),
                    borderSide: new BorderSide(),
                  ),
                ),
              ),
            )),
            Padding(padding: const EdgeInsets.all(10)),
            Expanded(
                child: SizedBox(
              height: 55,
              width: (MediaQuery.of(context).size.width - 56.0 - 10.0) / 2.0,
              child: TextFormField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  hintText: "Amount",
                  border: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(15.0),
                    borderSide: new BorderSide(),
                  ),
                ),
              ),
            ))
          ]),
        ],
      ),
    );
  }
}
