import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rocketPartner/Model/predefinedFoods.dart';
import 'package:rocketPartner/Presentation/Menu/components/show_menu_list.dart';
import 'package:rocketPartner/Presentation/Order/components/all_order_body.dart';
import 'package:rocketPartner/Presentation/Order/components/all_order_cards.dart';
import 'package:rocketPartner/Router/routeName.dart';
import '../../order/components/all_order_cards.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import 'add_menu_list.dart';

/// ignore: camel_case_types
class Body extends StatefulWidget {
  const Body({Key key}) : super(key: key);
  @override
  _BodyState createState() => _BodyState();
}

// ignore: camel_case_types
class _BodyState extends State<Body> with SingleTickerProviderStateMixin {
  ScrollController _scrollViewController;
  TabController _controller;
  int index = 0;
  List<dynamic> responseList = FOOD_DATA;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    // Create TabController for getting the index of current tab
    _controller = TabController(length: 2, vsync: this);
    _scrollViewController = ScrollController(initialScrollOffset: 0.0);
    _controller.addListener(() {
      print("Selected Index: " + _controller.index.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        drawer: Drawer(),
        floatingActionButton: FloatingActionButton(
            onPressed: () {},
            tooltip: 'Increment',
            child: IconButton(
                icon: Icon(Icons.add),
                onPressed: () {
                  Get.toNamed(RouteName.addMenuList);
                })
            //const Icon(Icons.add),
            ),

        // Colors.blue,
        body: SafeArea(
            left: false,
            top: true,
            bottom: false,
            right: false,
            child: NestedScrollView(
                controller: _scrollViewController,
                headerSliverBuilder:
                    (BuildContext context, bool boxIsScrolled) {
                  return <Widget>[
                    SliverAppBar(
                      pinned: true,
                      floating: true,
                      forceElevated: boxIsScrolled,

                      // here the desired height

                      backgroundColor: Colors.white,
                      actions: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 0.0, 30, 0.0),
                          child: Icon(
                            Icons.notifications,
                            color: Colors.green,
                            size: 30,
                          ),
                        )
                      ],
                      title: Center(
                          child: Text(
                        "Menu",
                        style: TextStyle(color: Colors.green, fontSize: 20),
                      )),
                      iconTheme: IconThemeData(color: Colors.green),
                      bottom: TabBar(
                        labelStyle: TextStyle(fontSize: 15.0),
                        unselectedLabelStyle: TextStyle(fontSize: 12.0),

                        indicator: BoxDecoration(color: Colors.green[50]
                            // color: _hasBeenPressed ? Color(0xffffffff) : Color(0xffff00a8),
                            ),
                        unselectedLabelColor: Colors.green[50],
                        indicatorColor: Colors.green[50],
                        //overlayColor: Colors.yellow,
                        controller: _controller,
                        tabs: [
                          Tab(
                              child: Text(
                            " Confirm ",
                            style: TextStyle(color: Colors.black),
                          )),
                          Tab(
                            child: Text(
                              "MenuList",
                              style: TextStyle(color: Colors.black),
                            ),
                          ),
                        ],
                      ),
                    )
                  ];
                },
                body: TabBarView(controller: _controller, children: [
                  Container(
                    // height: 130,
                    // width: MediaQuery.of(context).size.width,
                    // margin: EdgeInsets.symmetric(
                    //   horizontal: 16,
                    //   vertical: 8,
                    // ),
                    child: SingleChildScrollView(
                        child: Container(
                      color: Colors.white,
                      padding: const EdgeInsets.all(0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          AllOrderBody(),
                        ],
                      ),
                    )),
                  ),
                  Container(color: Colors.white, child: ShowMenuList()),
                ]))));
  }
}
