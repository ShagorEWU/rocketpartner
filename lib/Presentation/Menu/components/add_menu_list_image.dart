import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:get/route_manager.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

import 'package:rocketPartner/Router/routeName.dart';

var compressedFile;

class AddMenuListImage extends StatefulWidget {
  @override
  _AddMenuListImageState createState() => _AddMenuListImageState();
}

class _AddMenuListImageState extends State<AddMenuListImage> {
  File imageFile;

  Future _getImage(int type) async {
    var image = await ImagePicker().getImage(
        source: type == 1 ? ImageSource.camera : ImageSource.gallery,
        imageQuality: 50);

    File croppedFile = await ImageCropper.cropImage(
      sourcePath: image.path,
      maxWidth: 600,
      maxHeight: 600,
    );

    var compressedFile = await FlutterImageCompress.compressAndGetFile(
      croppedFile.path,
      croppedFile.path,
      quality: 50,
    );

    setState(() {
      imageFile = compressedFile;
    });
  }

  @override
  Widget build(BuildContext context) {
    var imageFile;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Get.back();
            }),
      ),
      body: Column(
        children: [
          Row(
            children: [
              imageFile != null
                  ? Image.file(
                      imageFile,
                      height: MediaQuery.of(context).size.height / 2,
                    )
                  : Text("Image editor"),

              Expanded(
                child: FlatButton(
                  child: Text("Camera"),
                  shape: RoundedRectangleBorder(),
                  color: Colors.blueAccent,
                  textColor: Colors.white,
                  onPressed: () {
                    _getImage(1);
                    Get.back();
                  },
                ),
              ),
              Expanded(
                child: FlatButton(
                  child: Text("Add Image"),
                  shape: RoundedRectangleBorder(),
                  color: Colors.blueAccent,
                  textColor: Colors.white,
                  onPressed: () {
                    _getImage(2);
                    Get.back();
                  },
                ),
              )
              //Image.file(croppedImage),
            ],
          ),
        ],
      ),
      bottomNavigationBar: SizedBox(
        height: ScreenUtil().setHeight(50), // specific value
        child: RaisedButton(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0)),
              side: BorderSide(color: Colors.green)),
          onPressed: () {
            Get.offNamed(RouteName.menuScreen);
            //print(_loginBloc.emailTextController.text);
          },
          color: Colors.green,
          textColor: Colors.white,
          child: Text('Continue'),

          //

          //
        ),
      ),
    );
  }
}
