import 'package:flutter/material.dart';
import 'package:rocketPartner/Common/Widgets/BottomNavBar/bottomNavBar.dart';
import 'package:rocketPartner/Presentation/Menu/components/body.dart';

// ignore: camel_case_types
class MenuScreen extends StatefulWidget {
  final BuildContext menuScreenContext;
  final Function onScreenHideButtonPressed;
  final bool hideStatus;
  const MenuScreen(
      {Key key,
      this.menuScreenContext,
      this.onScreenHideButtonPressed,
      this.hideStatus = false})
      : super(key: key);

  @override
  _MenuScreenState createState() => _MenuScreenState();
}

// ignore: camel_case_types
class _MenuScreenState extends State<MenuScreen> {
  static int index = 1;
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(),
      bottomNavigationBar: BottomNavBar(
        activeBar: index,
      ),
    );
  }
}
