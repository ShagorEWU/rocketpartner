import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'package:rocketPartner/API/Repository/RegistrationRepository.dart';
import 'package:rocketPartner/Common/Methods/Notifications.dart';
import 'package:rocketPartner/Model/RegistrationModel/RegistrationModel.dart';
import 'package:rocketPartner/Router/routeName.dart';

class RegistrationBloc extends GetxController {
  RegistrationRepository repository = Get.find();
  Notifications notifications = Get.find();

  @override
  void onInit() {
    super.onInit();
  }

  void submitRegistrationData(Map<String, dynamic> map) async {
    notifications.Loader(msg: "Registration On Going");
    RegistrationModel response = await repository.checkRegistration(map);

    if (response.errorCode == "0") {
      notifications.dismissLoader();
      Get.toNamed(RouteName.optScreen);
    } else {
      notifications.dismissLoader();

      notifications.snackBarDialog(
          message: "Invalid Input",
          icon: FlutterIcons.warning_faw,
          position: SnackPosition.TOP);
    }

    @override
    void onClose() {
      super.onClose();
    }
  }
}
