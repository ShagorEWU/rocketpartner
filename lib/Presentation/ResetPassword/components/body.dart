import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rocketPartner/Common/Variables/TextList.dart';

import 'reset_password_form_card.dart';

// ignore: camel_case_types
class body extends StatefulWidget {
  @override
  _bodyState createState() => _bodyState();
}

// ignore: camel_case_types
class _bodyState extends State<body> {
  bool _isSelected = false;

  void _radio() {
    setState(() {
      _isSelected = !_isSelected;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: true,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: 28.0, right: 28.0, top: 60.0),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: ScreenUtil().setHeight(0.0),
                  ),
                  resetFormCard(), //Reset Form Card
                  SizedBox(height: ScreenUtil().setHeight(40)),
                  //Login Form Card
                  Center(
                    child: SizedBox(
                      height: ScreenUtil().setHeight(50),
                      child: Center(
                        child: Row(
                          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            InkWell(
                              child: Container(
                                // width: ScreenUtil().setWidth(300),
                                // height: ScreenUtil().setHeight(100),
                                // padding: EdgeInsets.only(left: 44.0),
                                width: MediaQuery.of(context).size.width - 56.0,

                                decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.circular(45.0),
                                ),
                                child: Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                    onTap: () {
                                      Navigator.pushNamed(context, "/otp");
                                    },
                                    child: Center(
                                      child: CustomText.textContinue,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
