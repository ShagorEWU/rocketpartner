import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rocketPartner/Common/Widgets/Others/InputText.dart';

// ignore: camel_case_types
class resetFormCard extends StatefulWidget {
  @override
  _resetFormCardState createState() => _resetFormCardState();
}

// ignore: camel_case_types
class _resetFormCardState extends State<resetFormCard> {
  bool _isSelected = false;

  void _radio() {
    setState(() {
      _isSelected = !_isSelected;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      width: double.infinity,
//      height: ScreenUtil.getInstance().setHeight(500),
      padding: EdgeInsets.only(bottom: 1),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Padding(
        padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 00.0),
              child: Image.asset("asset/login.jpg"),
            ),
            SizedBox(
              height: ScreenUtil().setHeight(30),
            ),
            InputText(
              txt: "New password",
              textController: null,
            ),
            SizedBox(
              height: ScreenUtil().setHeight(35),
            ),
            InputText(
              txt: "password",
              textController: null,
            ),
            SizedBox(
              height: ScreenUtil().setHeight(35),
            ),
            InputText(
              txt: "Confirmspassword",
              textController: null,
            ),
            SizedBox(
              height: ScreenUtil().setHeight(35),
            ),
          ],
        ),
      ),
    );
  }
}
