import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rocketPartner/Common/Widgets/BottomNavBar/bottomNavBar.dart';

import 'components/body.dart';

class ProfileScreen extends StatelessWidget {
  //static String routeName = "/profile";
  static int index = 3;
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
          statusBarColor: Colors.white,
          statusBarIconBrightness: Brightness.dark),
    );
    return Scaffold(
      body: Body(),
      bottomNavigationBar: BottomNavBar(
        activeBar: index,
      ),
    );
  }
}
