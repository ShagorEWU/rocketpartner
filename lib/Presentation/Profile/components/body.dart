import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'package:rocketPartner/Common/Widgets/BottomNavBar/bottomNavBar.dart';
import 'package:rocketPartner/Router/routeName.dart';

import 'profile_menu.dart';
import 'profile_pic.dart';
import '../../../Router/routes.dart';
import '../../../Router/routeName.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        //padding: EdgeInsets.symmetric(vertical: 20),
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            Center(
              child: Text(
                "Profile",
                style: TextStyle(color: Colors.green, fontSize: 30),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            ProfilePic(),
            SizedBox(height: 20),
            InkWell(
              child: Container(
                // width: ScreenUtil().setWidth(300),
                // height: ScreenUtil().setHeight(100),
                // padding: EdgeInsets.only(left: 44.0),
                width: MediaQuery.of(context).size.width - 50.0,
                height: 70,

                decoration: BoxDecoration(
                  color: Color(0xFFCFD3E0),
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () {
                      Get.toNamed("/accountInformation");
                      // Navigator.pushNamed(context, RouteName.accountInformation);
                    },
                    child: Container(
                      //padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                      child: Row(
                        children: [
                          SizedBox(width: 40),
                          Expanded(
                              child: Text(
                            "Account Information",
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          )),
                          SizedBox(width: 60),
                          Expanded(
                            child: Icon(
                              Icons.arrow_forward,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 20),
            InkWell(
              child: Container(
                // width: ScreenUtil().setWidth(300),
                // height: ScreenUtil().setHeight(100),
                // padding: EdgeInsets.only(left: 44.0),
                width: MediaQuery.of(context).size.width - 50.0,
                height: 70,

                decoration: BoxDecoration(
                  color: Color(0xFFCFD3E0),
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () {
                      Get.toNamed("/accountInformation");
                      // Navigator.pushNamed(context, RouteName.accountInformation);
                    },
                    child: Container(
                      //padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                      child: Row(
                        children: [
                          SizedBox(width: 40),
                          Expanded(
                              child: Text(
                            "Setting",
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          )),
                          SizedBox(width: 60),
                          Expanded(
                            child: Icon(
                              FlutterIcons.setting_ant,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 20),
            InkWell(
              child: Container(
                // width: ScreenUtil().setWidth(300),
                // height: ScreenUtil().setHeight(100),
                // padding: EdgeInsets.only(left: 44.0),
                width: MediaQuery.of(context).size.width - 50.0,
                height: 70,

                decoration: BoxDecoration(
                  color: Color(0xFFCFD3E0),
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () {
                      Get.toNamed("/accountInformation");
                      // Navigator.pushNamed(context, RouteName.accountInformation);
                    },
                    child: Container(
                      //padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                      child: Row(
                        children: [
                          SizedBox(width: 40),
                          Expanded(
                              child: Text(
                            "Help Center",
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          )),
                          SizedBox(width: 60),
                          Expanded(
                            child: Icon(
                              FlutterIcons.profile_ant,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 20),
            InkWell(
              child: Container(
                // width: ScreenUtil().setWidth(300),
                // height: ScreenUtil().setHeight(100),
                // padding: EdgeInsets.only(left: 44.0),
                width: MediaQuery.of(context).size.width - 50.0,
                height: 70,

                decoration: BoxDecoration(
                  color: Color(0xFFCFD3E0),
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () {
                      Get.toNamed("/accountInformation");
                      // Navigator.pushNamed(context, RouteName.accountInformation);
                    },
                    child: Container(
                      //padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                      child: Row(
                        children: [
                          SizedBox(width: 40),
                          Expanded(
                              child: Text(
                            "Log Out",
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          )),
                          SizedBox(width: 60),
                          Expanded(
                            child: Icon(
                              FlutterIcons.logout_ant,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
