import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'dart:core';
import 'package:lite_rolling_switch/lite_rolling_switch.dart';
import 'package:get/get.dart';
import 'package:rocketPartner/Common/Storage/storageKey.dart';
import 'package:rocketPartner/Common/Storage/storagePref.dart';
import 'package:rocketPartner/Controller/User.dart';
import 'package:rocketPartner/Presentation/Profile/components/settings_informations.dart';

class ProfileMenu extends StatefulWidget {
  const ProfileMenu({
    Key key,
    @required this.txt,
    @required this.iconn,
  }) : super(key: key);

  final String txt;
  final Icon iconn;

  @override
  _ProfileMenuState createState() => _ProfileMenuState();
}

class _ProfileMenuState extends State<ProfileMenu> {
  DragStartBehavior dragStartBehaviorState = DragStartBehavior.start;
  bool _lights = false;
  bool status = true;
  Icon requiredIcon;
  int index = 0;
  List<bool> isSelected = [true, false];
  StoragePref storagePref = Get.find();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: FlatButton(
        padding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        color: Color(0xFFF5F6F9),
        onPressed: () {},
        child: Row(
          children: [
            SizedBox(width: 20),
            Expanded(child: Text(widget.txt)),
            widget.txt == "userName"
                ? Text(storagePref.storageRead(StorageKey.userNameKey))
                : widget.txt == "email"
                    ? Text(storagePref.storageRead(StorageKey.emailKey))
                    : widget.txt == "phoneNumber"
                        ? Text(
                            storagePref.storageRead(StorageKey.phoneNumberKey))
                        : widget.txt == "Logout"
                            ? IconButton(
                                icon: widget.iconn,
                                onPressed: () {
                                  Get.toNamed("/login");
                                },
                              )
                            : widget.txt == "Settings"
                                ? IconButton(
                                    icon: widget.iconn,
                                    onPressed: () {
                                      Get.bottomSheet(Container(
                                        color: Colors.green[50],
                                        child: SizedBox(
                                          height: 300,
                                          child: SingleChildScrollView(
                                              //controller: _controller,
                                              child: Column(
                                            children: [
                                              settings_informations(
                                                  txt: "Notifications"),
                                              settings_informations(
                                                  txt: "ActiveStatus"),
                                              settings_informations(
                                                  txt: "DarkMode"),
                                            ],
                                          )),
                                        ),
                                      ));
                                    },
                                  )
                                : IconButton(
                                    icon: widget.iconn,
                                    onPressed: () {},
                                  ),
          ],
        ),
      ),
    );
  }
}
