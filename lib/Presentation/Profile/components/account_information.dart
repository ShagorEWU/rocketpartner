import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:core';

import 'package:get/get.dart';
import 'package:rocketPartner/Common/Storage/storageKey.dart';
import 'package:rocketPartner/Common/Storage/storagePref.dart';

class AccountInformation extends StatefulWidget {
  @override
  _AccountInformationState createState() => _AccountInformationState();
}

class _AccountInformationState extends State<AccountInformation> {
  StoragePref storagePref = Get.find();

  @override
  Widget build(BuildContext context) {
    // SystemUiOverlayStyle(
    //   statusBarColor: Colors.white,
    //   statusBarIconBrightness: Brightness.light,
    // );

    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            leading: IconButton(
                color: Colors.black,
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Get.back();
                }),
            title: new Center(
                child: new Text("Account Information",
                    style: TextStyle(color: Colors.black),
                    textAlign: TextAlign.center)),
          ),
          body: Container(
            padding: const EdgeInsets.all(20),
            child: Card(
              child: Column(
                children: [
                  SizedBox(
                    height: 30.0,
                  ),
                  Card(
                    child: Container(
                      height: 60,
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("userName"),
                          ),
                          Expanded(
                            child: Text(storagePref
                                .storageRead(StorageKey.userNameKey)),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  Card(
                    child: Container(
                      height: 60,
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("email"),
                          ),
                          Expanded(
                              child: Text(storagePref
                                  .storageRead(StorageKey.emailKey))),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  Card(
                    child: Container(
                      height: 60,
                      child: Row(
                        children: [
                          Expanded(
                            child: Text("phoneNumber"),
                          ),
                          Expanded(
                              child: Text(storagePref
                                  .storageRead(StorageKey.phoneNumberKey))),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
