import 'package:flutter/material.dart';
import 'package:rocketPartner/Presentation/Order/components/all_order_cards.dart';

class AllOrderBody extends StatefulWidget {
  @override
  _AllOrderBodyState createState() => _AllOrderBodyState();
}

class _AllOrderBodyState extends State<AllOrderBody> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.only(bottom: 50.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          all_order_cards(),
        ],
      ),
    );
  }
}
