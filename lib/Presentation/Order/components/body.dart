import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rocketPartner/Common/Methods/HexColor.dart';
import 'package:rocketPartner/Common/Widgets/AppBar/appBar.dart';
import 'package:rocketPartner/Common/Widgets/BottomNavBar/bottomNavBar.dart';
import 'package:rocketPartner/Presentation/Order/components/all_order_body.dart';
import 'package:rocketPartner/Presentation/Order/components/tabbar_body.dart';
import 'all_order_cards.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

// ignore: camel_case_types
class Body extends StatefulWidget {
  const Body({
    Key key,
  }) : super(key: key);
  @override
  _BodyState createState() => _BodyState();
}

// ignore: camel_case_types
class _BodyState extends State<Body> with SingleTickerProviderStateMixin {
  ScrollController _scrollViewController;
  TabController _controller;
  int index = 0;
  int counter = 5;
  double height = 0;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  void _openEndDrawer() {
    _scaffoldKey.currentState.openEndDrawer();
  }

  void _closeEndDrawer() {
    Navigator.of(context).pop();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    height = 0;
    // Create TabController for getting the index of current tab
    _controller = TabController(length: 3, vsync: this);
    _scrollViewController = ScrollController(initialScrollOffset: 0.0);
    _controller.addListener(() {
      print("Selected Index: " + _controller.index.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Container(
          width: 350,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Drawer(
              child: ListView(
                padding: EdgeInsets.zero,
                children: <Widget>[
                  UserAccountsDrawerHeader(
                    accountName: Text('Test123'),
                    accountEmail: Text('test@123.com'),
                    currentAccountPicture: Image.network(
                        'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/a89c3e38-b6f3-48a0-9f9e-df9a0129fb93/daghh5x-4a77b3ec-fd4f-4d17-9f84-5963a8cb5c03.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2E4OWMzZTM4LWI2ZjMtNDhhMC05ZjllLWRmOWEwMTI5ZmI5M1wvZGFnaGg1eC00YTc3YjNlYy1mZDRmLTRkMTctOWY4NC01OTYzYThjYjVjMDMucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.dWTFMrwnbAbj5TtUp9U_vQsohW7MnkRPymzR5wZQoV8'),
                  ),
                  ListTile(
                    title: Text('data'),
                  ),
                ],
              ),
            ),
          ),
        ),
        endDrawer: Padding(
          padding: EdgeInsets.only(top: height),
          child: Drawer(
            child: Padding(
              padding: const EdgeInsets.all(40),
              child: Container(
                color: Colors.red,
              ),
            ),
          ),
        ),
        // Disable opening the end drawer with a swipe gesture.
        endDrawerEnableOpenDragGesture: false,
        body: SafeArea(
            left: false,
            top: true,
            bottom: true,
            right: false,
            child: NestedScrollView(
                controller: _scrollViewController,
                headerSliverBuilder:
                    (BuildContext context, bool boxIsScrolled) {
                  return <Widget>[
                    // ignore: missing_required_param
                    PreferredSize(
                      preferredSize: Size.fromHeight(120.0),
                      child: SliverAppBar(
                        pinned: true,
                        floating: true,
                        forceElevated: boxIsScrolled,

                        // here the desired height

                        backgroundColor: Colors.white,
                        actions: [
                          //Notifications  Show
                          new Stack(
                            children: <Widget>[
                              new Builder(
                                builder: (context) => IconButton(
                                  padding:
                                      const EdgeInsets.fromLTRB(0, 16, 16, 0),
                                  icon: Icon(Icons.notifications),
                                  onPressed: () {
                                    setState(() {
                                      height = 120.0;
                                    });
                                    Scaffold.of(context).openEndDrawer();
                                  },
                                  tooltip: MaterialLocalizations.of(context)
                                      .openAppDrawerTooltip,
                                ),
                              ),
                              counter != 0
                                  ? new Positioned(
                                      right: 11,
                                      top: 11,
                                      child: new Container(
                                        padding: EdgeInsets.all(2),
                                        decoration: new BoxDecoration(
                                          color: Colors.red,
                                          borderRadius:
                                              BorderRadius.circular(6),
                                        ),
                                        constraints: BoxConstraints(
                                          minWidth: 14,
                                          minHeight: 14,
                                        ),
                                        child: Text(
                                          '$counter',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 8,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    )
                                  : new Container()
                            ],
                          ),
                        ],
                        iconTheme: IconThemeData(color: Colors.green),
                        title: Center(
                            child: Padding(
                          padding: const EdgeInsets.only(bottom: 16.0),
                          child: Text(
                            "Order",
                            style: TextStyle(color: Colors.green, fontSize: 20),
                          ),
                        )),

                        bottom: PreferredSize(
                          preferredSize: Size.fromHeight(50.0),
                          child: TabBarBody(
                            activeBar: _controller,
                          ),
                        ),
                      ),
                    )
                  ];
                },
                body: TabBarView(controller: _controller, children: [
                  SingleChildScrollView(child: AllOrderBody()),
                  SingleChildScrollView(child: AllOrderBody()),
                  SingleChildScrollView(child: AllOrderBody()),
                ]))));
  }
}
