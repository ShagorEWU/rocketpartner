import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
//import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rocketPartner/Presentation/Menu/menu_screen.dart';
import 'package:rocketPartner/Presentation/Order/order_screen.dart';
import 'package:rocketPartner/Presentation/Profile/profile_screen.dart';

BuildContext testContext;

class TabBarBody extends StatefulWidget {
  const TabBarBody({
    Key key,
    @required this.activeBar,
  }) : super(key: key);

  final TabController activeBar;
  @override
  _TabBarBodyState createState() => _TabBarBodyState();
}

class _TabBarBodyState extends State<TabBarBody> {
  TabController _controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = widget.activeBar;
  }

  @override
  Widget build(BuildContext context) {
    return TabBar(
      labelStyle: TextStyle(fontSize: 15.0),
      unselectedLabelStyle: TextStyle(fontSize: 12.0),
      //isScrollable: true,
      //indicator: ,

      //For Un-selected Tabs
      indicator: BoxDecoration(color: Colors.green[50]
          // color: _hasBeenPressed ? Color(0xffffffff) : Color(0xffff00a8),
          ),
      unselectedLabelColor: Colors.green[50],
      indicatorColor: Colors.green[50],

      //automaticIndicatorColorAdjustment: true,
      controller: _controller,
      tabs: [
        Tab(
            child: Text(
          "Ongoing",
          style: TextStyle(color: Colors.black),
        )),
        Tab(
          child: Text(
            "Delivered",
            style: TextStyle(color: Colors.black),
          ),
        ),
        Tab(
          child: Text(
            "Cancelled",
            style: TextStyle(color: Colors.black),
          ),
        ),
      ],
    );
  }
}
