import 'package:flutter/material.dart';
import 'package:rocketPartner/Common/Widgets/BottomNavBar/bottomNavBar.dart';
import 'package:rocketPartner/Presentation/Order/components/body.dart';

// ignore: camel_case_types
class OrderScreen extends StatefulWidget {
  const OrderScreen({
    Key key,
  }) : super(key: key);

  @override
  _OrderScreenState createState() => _OrderScreenState();
}

// ignore: camel_case_types
class _OrderScreenState extends State<OrderScreen> {
  static int index = 0;
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(),
      bottomNavigationBar: BottomNavBar(
        activeBar: index,
      ),
    );
  }
}
