import 'package:flutter/material.dart';

import 'otp_digital_form_card.dart';

// ignore: camel_case_types
class otp_form_card extends StatefulWidget {
  @override
  _otp_form_cardState createState() => _otp_form_cardState();
}

// ignore: camel_case_types
class _otp_form_cardState extends State<otp_form_card> {
  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.transparent,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(child: OTPDigitTextFieldBox(first: true, last: false)),
                Padding(
                  padding: const EdgeInsets.all(5),
                ),
                Expanded(
                    child: OTPDigitTextFieldBox(first: false, last: false)),
                Padding(
                  padding: const EdgeInsets.only(left: 5),
                ),
                Expanded(
                    child: OTPDigitTextFieldBox(first: false, last: false)),
                Padding(
                  padding: const EdgeInsets.only(left: 5),
                ),
                Expanded(child: OTPDigitTextFieldBox(first: false, last: true)),
              ],
            )
          ]),
        ));
  }
}
